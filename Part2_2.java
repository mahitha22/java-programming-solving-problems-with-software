
/**
 * Write a description of Part2 here.
 * 
 * @author Mahitha
 * Strings Assignment 2 - Part 2
 * @version (a version number or a date)*/
public class Part2_2 {

    public int howMany(String stringa, String stringb) {
        int lastIndex = 0;
        int count = 0;
        while(true) {
        int index = stringb.indexOf(stringa,lastIndex);
        if(index == -1) {
            break;
        }
        else {
        lastIndex = stringb.indexOf(stringa,index) + stringa.length();
            count ++;
        }
    }
    return count;
}
public void testHowMany() {
        
        int count = howMany("an","banana");
        System.out.println(count);
        count = howMany("AA","ATAAAA");
        System.out.println(count);
        count = howMany("GAA","ATGAACGAATTGAATC");
        System.out.println(count);
        count = howMany("how","areyou");
        System.out.println(count);
    }
}

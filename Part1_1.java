 

import edu.duke.*;
/**
 * Write a description of Part1 here.
 * 
 * @author Mahitha
 * Strings Assignment1 Part 1
 * @version (a version number or a date)
 */
public class Part1_1 {
    public String findSimpleGene(String dna) {
        int startCodonIndex = 0;
        int stopCodonIndex = 0;
        startCodonIndex = dna.indexOf("ATG");
        if (startCodonIndex == -1) {
            return "No start codon";
        }
        stopCodonIndex = dna.indexOf("TAA", startCodonIndex + 3);
        if (stopCodonIndex == -1) {
            return "No stop codon";
        }
        if ((stopCodonIndex - startCodonIndex) % 3 == 0) {
            return dna.substring(startCodonIndex, stopCodonIndex + 3);
        }
        else 
        return "not a multiple of 3";
    }
    public void testSimpleGene() {
        String gene = "";
        String DNA = "ATGTAA";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("The gene is: "+gene);
        DNA = "ATGTTCTTFTTGTAA";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("The gene is: "+gene);
        DNA = "TTCTTFTTGTAA";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("The gene is: "+gene);
        DNA = "ATGTTCTTFTTG";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("The gene is: "+gene);
        DNA = "ATGTTCTTFTTGTTTAA";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("The gene is: "+gene);
        DNA = "ATTTCTTFTTGTTTTF";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("The gene is: "+gene);
    }
    public static void main(String args[]) {
        //Part1 p1 = new Part1();
        //p1.testSimpleGene();
        //Part2 p2 = new Part2();
        //p2.testSimpleGene();
        //Part3 p3 = new Part3();
        //p3.testing();
        Part4 p4 = new Part4();
        p4.printURL();
    }
}

class Part2 {
    public String findSimpleGene(String dna, String startCodon, String stopCodon) {
        int startCodonIndex = 0;
        int stopCodonIndex = 0;
        String upperDNA = dna.toUpperCase();
        startCodonIndex = upperDNA.indexOf(startCodon);
        if (startCodonIndex == -1) {
            return "No start codon";
        }
        stopCodonIndex = upperDNA.indexOf(stopCodon, startCodonIndex);
        if (stopCodonIndex == -1) {
            return "No stop codon";
        }
        if ((stopCodonIndex - startCodonIndex) % 3 == 0) {
            return dna.substring(startCodonIndex, stopCodonIndex + 3);
        }
        else 
        return "not a multiple of 3";
    }
    public void testSimpleGene() {
        String gene = "";
        String DNA = "ATGTAA";
        System.out.println("Part2");
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "ATGTTCTTFTTGTAA";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "TTCTTFTTGTAA";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "ATGTTCTTFTTG";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "ATGTTCTTFTTGTTTAA";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "ATTTCTTFTTGTTTTF";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        
        DNA = "atgtaa";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "atgttcttfttgtaa";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "ttcttdttgtaa";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "atgttcttfttg";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "atgttcttfttgtttaa";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "atttcttfttgttttf";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
        DNA = "ATGtffATGtaftaa";
        System.out.println("The DNA String is: "+DNA);
        gene = findSimpleGene(DNA,"ATG","TAA");
        System.out.println("The gene is: "+gene);
    }
}
class Part3 {
    public boolean twooccurences(String stringa, String stringb) {
      int firstOccurence = stringb.indexOf(stringa);
      if (firstOccurence != -1) {
          if(stringb.indexOf(stringa, firstOccurence+stringa.length()) != -1) {
              return true;
          }
        }
        return false;
    }
    public void testing() {
        String a = "by";
        String b = "A story by Abby long";
        boolean result;
        result = twooccurences(a,b);
        System.out.println("The strings are: "+a+", "+b);
        System.out.println("Result: "+result);
        System.out.println("The last part result: "+lastPart(a,b));
        a = "a";
        b = "banana";
        result = twooccurences(a,b);
        System.out.println("The strings are: "+a+", "+b);
        System.out.println("Result: "+result);
        System.out.println("The last part result: "+lastPart(a,b));
        a= "atg";
        b = "ctgtatgta";
        result = twooccurences(a,b);
        System.out.println("The strings are: "+a+", "+b);
        System.out.println("Result: "+result);
        System.out.println("The last part result: "+lastPart(a,b));
        a = "hell";
        b = "hello";
        result = twooccurences(a,b);
        System.out.println("The strings are: "+a+", "+b);
        System.out.println("Result: "+result);
        System.out.println("The last part result: "+lastPart(a,b));
        a = "hell";
        b = "hellhello";
        result = twooccurences(a,b);
        System.out.println("The strings are: "+a+", "+b);
        System.out.println("Result: "+result);
        System.out.println("The last part result: "+lastPart(a,b));  
        a = "zoo";
        b = "forest";
        result = twooccurences(a,b);
        System.out.println("The strings are: "+a+", "+b);
        System.out.println("Result: "+result);
        System.out.println("The last part result: "+lastPart(a,b)); 
    }
    public String lastPart(String stringa, String stringb) {
        int firstOccurence = stringb.indexOf(stringa);
        if (firstOccurence != -1) {
            return stringb.substring(firstOccurence + stringa.length());
        }
        else {
            return stringb;
        }
    }
}
class Part4 {
    public void printURL(){
        URLResource ur = new URLResource("http://www.dukelearntoprogram.com/course2/data/manylinks.html");
        String s = "youtube.com";
        for (String word : ur.words()) {
            String lowerWord = word.toLowerCase();
            int index = lowerWord.indexOf(s);
            if (index != -1) {
                int last = word.indexOf("\"", index+s.length());
                int first = word.lastIndexOf("\"",index+s.length());
                System.out.println(word.substring(first+1,last));
            }
       }
    }   
}
 

import edu.duke.*;
/**
 * Write a description of Part1 here.
 * 
 * @author Mahitha
 * Strings Assignment 3 Part1 
 * @version (a version number or a date)
 */
public class Part3_1 {

public int findStopCodon (String dna, int startIndex, String stopCodon) {
 int currIndex = dna.indexOf(stopCodon,startIndex + 3);
 while(currIndex != -1) {
  if((currIndex - startIndex) % 3 == 0) {
   return currIndex;
  }
  else {
    currIndex = dna.indexOf(stopCodon,currIndex+1);    
  }
}
return -1;
}

public  String findSimpleGene(String dna, int where) {
    int startIndex = dna.indexOf("ATG", where);
    if(startIndex == -1) {
      return "";
    }
    int taaIndex = findStopCodon(dna, startIndex, "TAA");
    int tgaIndex = findStopCodon(dna, startIndex, "TGA");
    int tagIndex = findStopCodon(dna, startIndex, "TAG");
    int minIndex = 0;
    if(taaIndex == -1 || (tgaIndex != -1 && tgaIndex < taaIndex)) {
        minIndex = tgaIndex;
    }
    else {
        minIndex = taaIndex;
    }
    if(minIndex == -1 || (tagIndex != -1 && tagIndex < minIndex)) {
        minIndex = tagIndex;
    }
  
    if(minIndex == -1) {
       return ""; 
    }
    return dna.substring(startIndex,minIndex + 3);
    
}
public StorageResource getAllGenes(String dna) {
    StorageResource geneList = new StorageResource();
    int startIndex = 0;
    while( true ){
        String currGene = findSimpleGene(dna, startIndex);
        if(currGene.isEmpty()) {
            break;
        }
        geneList.add(currGene);
        startIndex = dna.indexOf(currGene, startIndex) + currGene.length();
        //System.out.println(startIndex);
    }
    return geneList;
}
public void testOn() {
               //   =   =  =  =           -  -  -  -      9  9  9  9 
    //String DNA = "ATGATCCTATAAGHCVBNTYUATGHJUIKLTAGGHVCATGJHBGTTAATAG";
    FileResource fr = new FileResource();
    String dna = fr.asString();
    String dna1 = dna.toUpperCase();
    int count = 0;
    StorageResource genes = getAllGenes(dna1);
    for (String item : genes.data()) {
        count++;
    }
    System.out.println(count);
  }
public float cgRatio(String dna) {
    float ratio = 0.0F;
    int count = 0;
    for (int i = 0; i < dna.length(); i++) {
        if (dna.charAt(i) == 'C' || dna.charAt(i) == 'G') {
            count++;
        }
    }
    ratio = (float) count/dna.length();
    return ratio;
}
public int countCTG() {
    FileResource fr = new FileResource();
    String dna = fr.asString();
      int count = 0;
      int lastIndex = 0;
      String codon = "CTG";
      while(true) {
          int index = dna.indexOf(codon,lastIndex);
          if(index == -1) {
              break;
          }
          else {
              lastIndex = dna.indexOf(codon,index) + codon.length();
              count++;
          }
      }
      return count;
}
public void processGenes(StorageResource sr) {
    int lcount = 0;
    int rcount = 0;
    int length = 0;
    int ctgcount = 0;
    for (String item : sr.data()) {
        if(item.length() > 60) {
            System.out.println(item);
            lcount++;
        }
        float ratio = cgRatio(item);
        if(ratio > 0.35) {
            System.out.println(item);
            rcount++;
        }
        if(item.length() > length) {
            length = item.length();
        }
       // ctgcount += countCTG(item);
    }
    System.out.println("Number of strings with length greater than 60: "+lcount);
    System.out.println("Number of strings with cgRatio greater than 0.35: "+rcount++);
    System.out.println("Length of the longest gene: "+length);
    System.out.println("CTG Count: "+ctgcount);
}
public void testProcessGenes() {
    FileResource fr = new FileResource();
    String dna = fr.asString();
    String dna1 = dna.toUpperCase();
    StorageResource sr = getAllGenes(dna1);
    processGenes(sr);
}
}         
 

import edu.duke.*;
import org.apache.commons.csv.*;
import java.io.*;
/**
 * Write a description of weatherData here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class weatherData {
    public CSVRecord hottestHourInFile(CSVParser parser) {
        CSVRecord largestSoFar = null;
        for (CSVRecord record : parser) {
            largestSoFar = getLargestOfTwo (record, largestSoFar);
        }
        return largestSoFar;
    }
        
    public CSVRecord hottestInManyDays() {
        DirectoryResource dr = new DirectoryResource();
        CSVRecord largestSoFar = null;
        for (File f : dr.selectedFiles()) {
            FileResource fr = new FileResource(f);
            CSVRecord current = hottestHourInFile(fr.getCSVParser());
            largestSoFar = getLargestOfTwo (current, largestSoFar);
        }
        return largestSoFar;
    }
    
    public CSVRecord getLargestOfTwo (CSVRecord currentRow, CSVRecord largestSoFar) {
        if (largestSoFar == null) {
            largestSoFar = currentRow;
        }
        else {
            double temperature = Double.parseDouble(currentRow.get("TemperatureF"));
            double largestTemp = Double.parseDouble(largestSoFar.get("TemperatureF"));
            if (temperature > largestTemp) {
                largestSoFar = currentRow;
            }
        }
        return largestSoFar;
    }
    
    public CSVRecord getColdestOfTwo (CSVRecord currentRow, CSVRecord coldestSoFar) {
        if (coldestSoFar == null) {
            coldestSoFar = currentRow;
        }
        else {
            double currTemp = Double.parseDouble(currentRow.get("TemperatureF"));
            double coldestTemp = Double.parseDouble(coldestSoFar.get("TemperatureF"));
            if ((currTemp < coldestTemp) && (currTemp != -9999)) {
                coldestSoFar = currentRow;
            }
        }
        return coldestSoFar;
    }
        
    
    public CSVRecord coldestHourInFile (CSVParser parser) {
        CSVRecord coldestSoFar = null;
        for (CSVRecord currentRow : parser) {
            coldestSoFar = getColdestOfTwo(currentRow, coldestSoFar);
        }
        return coldestSoFar;
    }
    
    public void testHottestInDay() {
        FileResource fr = new FileResource();
        CSVRecord largest = hottestHourInFile(fr.getCSVParser());
        System.out.println("Hottest temperature was " + largest.get("TemperatureF") + " at " + largest.get("TimeEST"));
    }
    
    public void testHottestInManyDays() {
        CSVRecord largest = hottestInManyDays();
        System.out.println("Hottest temperature was "+ largest.get("TemperatureF") + " at " + largest.get("DateUTC"));
    }
    
    public void testColdestInDay() {
        FileResource fr = new FileResource();
        CSVRecord coldest = coldestHourInFile(fr.getCSVParser());
        System.out.println("Coldest Temperature was "+coldest.get("TemperatureF")+" at "+coldest.get("DateUTC"));
    }
    
    public String fileWithColdestTemperature() {
        DirectoryResource dr = new DirectoryResource();
        CSVRecord coldestSoFar1 = null;
        String filename = "";
        for (File f : dr.selectedFiles()) {
            FileResource fr = new FileResource(f);
            CSVRecord currentRow = coldestHourInFile(fr.getCSVParser());
            if (coldestSoFar1 == null) {
                coldestSoFar1 = currentRow;
                filename = f.getName();
            }
            else {
                double currTemp = Double.parseDouble(currentRow.get("TemperatureF"));
                double coldestTemp = Double.parseDouble(coldestSoFar1.get("TemperatureF"));
                if ((currTemp < coldestTemp) && (currTemp != -9999)) {
                    coldestSoFar1 = currentRow;
                    filename = f.getName();
                }
            }
        }
        //System.out.println("File name is : "+filename);
        return filename; 
    }
    
    public void testFileWithColdestTemperature() {
       String coldestFileName = fileWithColdestTemperature();
       String str[] = coldestFileName.split("-");
       String path = "C:\\Users\\dbtma\\OneDrive\\Documents\\BlueJProjects\\WeatherData\\nc_weather\\" + str[1] + "\\" +coldestFileName;
       FileResource fr = new FileResource(path);
       CSVRecord coldest = coldestHourInFile(fr.getCSVParser());
       System.out.println("Coldest day was in file "+coldestFileName);
       System.out.println("Coldest temperature on the day was "+ coldest.get("TemperatureF"));
       System.out.println("All the temperatures on the coldest day were: ");
       for (CSVRecord record : fr.getCSVParser()) {
            System.out.println(record.get("DateUTC")+" "+record.get("TemperatureF"));
        }
    }
    
    public CSVRecord lowestHumidityInFile(CSVParser parser) {
        CSVRecord lowestHumiditySoFar = null;
        for (CSVRecord currentRow : parser) {
            if(lowestHumiditySoFar == null) {
                if(!(currentRow.get("Humidity").equals("N/A"))) {
                    lowestHumiditySoFar = currentRow;
                }
            }
            else {
                if(!(currentRow.get("Humidity").equals("N/A"))) {
                    double currentHumidity = Double.parseDouble(currentRow.get("Humidity"));
                    double lowestHumidity = Double.parseDouble(lowestHumiditySoFar.get("Humidity"));
                    if(currentHumidity < lowestHumidity) {
                        lowestHumiditySoFar = currentRow;
                    }
                }
            }
        }
        return lowestHumiditySoFar;
    }
    
    public void testLowestHumidityInFile() {
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        CSVRecord csv = lowestHumidityInFile(parser);
        System.out.println("Lowest Humidity was " + csv.get("Humidity") +" at "+ csv.get("DateUTC"));
    }
    
    public CSVRecord lowestHumidityInManyFiles () {
        CSVRecord lowestHumiditySoFar = null;
        DirectoryResource dr = new DirectoryResource();
        for (File f : dr.selectedFiles()) {
            FileResource fr = new FileResource(f);
            CSVParser parser = fr.getCSVParser();
            CSVRecord currentRow = lowestHumidityInFile(parser);
            if(lowestHumiditySoFar == null) {
                if(!(currentRow.get("Humidity").equals("N/A"))) {
                    lowestHumiditySoFar = currentRow;
                }
            }
            else {
                if(!(currentRow.get("Humidity").equals("N/A"))) {
                    double currentHumidity = Double.parseDouble(currentRow.get("Humidity"));
                    double lowestHumidity = Double.parseDouble(lowestHumiditySoFar.get("Humidity"));
                    if(currentHumidity < lowestHumidity) {
                        lowestHumiditySoFar = currentRow;
                    }
                }
            }
        }
        return lowestHumiditySoFar;
    }
    
    public void testLowestHumidityInManyFiles() {
        CSVRecord record = lowestHumidityInManyFiles();
        System.out.println("Lowest Humidity was "+ record.get("Humidity")+ " at " + record.get("DateUTC"));
    }
    
    public double averageTemparatureInFile(CSVParser parser) {
        double avgTemp = 0.0;
        double temp = 0.0;
        int count = 0;
        for (CSVRecord record : parser) {
            temp += Double.parseDouble(record.get("TemperatureF"));
            count++;
        }
        return temp/count;
    }
    
    public void testAverageTemperatureInFile() {
        FileResource fr = new FileResource();
        double avgTemp =  averageTemparatureInFile(fr.getCSVParser());
        System.out.println("Average temperature in file is : "+avgTemp);
    }
    
    public double averageTemperatureWithHighHumidityInFile (CSVParser parser, int value) {
        double avg = 0.0;
        double temp = 0.0;
        int count = 0;
        for (CSVRecord record : parser) {
            if(!(record.get("Humidity").equals("N/A"))) {
                double humidity = Double.parseDouble(record.get("Humidity"));
                if (humidity >= (double)value) {
                    temp += Double.parseDouble(record.get("TemperatureF"));
                    count ++;
                }
            }
        }
        if(count != 0) {
            avg = temp/count;
        }
        return avg;
    }
    
    public void testAverageTemperatureWithHighHumidityInFile() {
        FileResource fr = new FileResource();
        double avgTemp =  averageTemperatureWithHighHumidityInFile(fr.getCSVParser(), 80);
        if (avgTemp == 0.0) {
            System.out.println("No temperatures with that humidity");
        }
        else {
            System.out.println("Average temperature in file is : "+avgTemp);
        }
    }
}

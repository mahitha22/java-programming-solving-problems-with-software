 
import edu.duke.*;
import org.apache.commons.csv.*;

/**
 * Write a description of Exports here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Exports {
    public void tester() {
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        String str = countryInfo(parser, "Nauru");
        System.out.println(str);
        parser = fr.getCSVParser();
        listExportersTwoProducts(parser, "cotton", "flowers");
        parser = fr.getCSVParser();
        System.out.println("Num of exporters: "+numberOfExporters(parser, "cocoa"));
        parser = fr.getCSVParser();
        bigExporters(parser, "$999,999,999,999");
    }
    public String countryInfo(CSVParser parser, String country) {
        for (CSVRecord record : parser) {
            String c = record.get("Country");
            if(c.equalsIgnoreCase(country)) {
                String str = c +":"+ record.get("Exports")+":"+record.get("Value (dollars)");
                return str;
            }
        }
        return "NOT FOUND";
    }
    public void listExportersTwoProducts(CSVParser parser, String exportItem1,String exportItem2) {
        for (CSVRecord record : parser) {
            String exports = record.get("Exports");
            if((exports.contains(exportItem1)) && (exports.contains(exportItem2))) {
                System.out.println(record.get("Country"));
            }
        }
    }
    public int numberOfExporters(CSVParser parser, String exportItem) {
        int count = 0;
        for (CSVRecord record : parser) {
            String exports = record.get("Exports");
            if(exports.contains(exportItem)) {
                count++;
            }
        }
        return count;
    }
    public void bigExporters(CSVParser parser, String amount) {
        int amountLen = amount.length();
        for (CSVRecord record : parser) {
            String amt = record.get("Value (dollars)");
            int length = amt.length();
            if(length > amountLen) {
                System.out.println(record.get("Country")+" "+amt);
            }
        }
    }
    public static void main(String args[]) {
        //Select a file from the exports data.
        Exports e = new Exports();
        e.tester();
    }
}

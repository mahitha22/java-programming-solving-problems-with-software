import edu.duke.*;
import java.io.File;

public class PerimeterAssignmentRunner {
    public double getPerimeter (Shape s) {
        // Start with totalPerim = 0
        double totalPerim = 0.0;
        // Start wth prevPt = the last point 
        Point prevPt = s.getLastPoint();
        // For each point currPt in the shape,
        for (Point currPt : s.getPoints()) {
            // Find distance from prevPt point to currPt 
            double currDist = prevPt.distance(currPt);
            // Update totalPerim by currDist
            totalPerim = totalPerim + currDist;
            // Update prevPt to be currPt
            prevPt = currPt;
        }
        // totalPerim is the answer
        return totalPerim;
    }
    public int getNumPoints (Shape s) {
        
        int count = 0;
        for (Point p : s.getPoints()) {
            count++;
        }
        return count;
    }

    public double getAverageLength(Shape s) {
        double avgLength = 0.0;
        double totalLength = getPerimeter(s);
        int numOfSides = getNumPoints(s);
        avgLength = totalLength / numOfSides;
        return avgLength;
    }

    public double getLargestSide(Shape s) {
        
        double largestSide = 0.0;
        Point prevPt = s.getLastPoint();
        // For each point currPt in the shape,
        for (Point currPt : s.getPoints()) {
            // Find distance from prevPt point to currPt 
            double currDist = prevPt.distance(currPt);
            // Update totalPerim by currDist
            if (currDist > largestSide) {
                largestSide = currDist;
            }
            // Update prevPt to be currPt
            prevPt = currPt;
        }
        return largestSide;
    }

    public double getLargestX(Shape s) {
        
        double largestX = 0.0;
        Point prevPt = s.getLastPoint();
        // For each point currPt in the shape,
        for (Point currPt : s.getPoints()) {
            //get x1 and x2 of the points
            double x1 = prevPt.getX();
            double x2 = currPt.getX();
            if (x1 > x2) {
                largestX = x1;
            }
            else {
                largestX = x2;
            }
            // Update prevPt to be currPt
            prevPt = currPt;
        }
        
        return largestX;
    }

    public double getLargestPerimeterMultipleFiles() {
        
        double largestPerimeter = 0.0;
        DirectoryResource dr = new DirectoryResource();
        for (File f : dr.selectedFiles()) {
            FileResource fr = new FileResource(f);
            Shape s = new Shape(fr);
            double perimeter = getPerimeter(s);
            if (largestPerimeter < perimeter) {
                largestPerimeter = perimeter;
            }
        }
        return largestPerimeter;
    }

    public String getFileWithLargestPerimeter() {
        
        File temp = null;    // replace this code
        double largestPerimeter = 0.0;
        DirectoryResource dr = new DirectoryResource();
        for (File f : dr.selectedFiles()) {
            FileResource fr = new FileResource(f);
            Shape s = new Shape(fr);
            double perimeter = getPerimeter(s);
            if (largestPerimeter < perimeter) {
                largestPerimeter = perimeter;
                temp = f;
            }
        }
        return temp.getName();
    }

    public void testPerimeterMultipleFiles() {
        
        System.out.println("The largest Perimeter is : "+ getLargestPerimeterMultipleFiles());
    }

    public void testFileWithLargestPerimeter() {
        
        System.out.println("The file which has largest Perimeter is : "+ getFileWithLargestPerimeter());
    }

    
    public void triangle(){
        Shape triangle = new Shape();
        triangle.addPoint(new Point(0,0));
        triangle.addPoint(new Point(6,0));
        triangle.addPoint(new Point(3,6));
        for (Point p : triangle.getPoints()){
            System.out.println(p);
        }
        double peri = getPerimeter(triangle);
        System.out.println("perimeter = "+peri);
    }
    public void testPerimeter() {
        FileResource fr = new FileResource();
        Shape s = new Shape(fr);
        //Shape s = new Shape();
        System.out.println("The total number of points are: "+ getNumPoints(s));
        System.out.println("Average length of all sides of the shape is: "+ getAverageLength(s));
        System.out.println("The largest side of the shape is: "+ getLargestSide(s));
        System.out.println("The largest X of the shape is: "+ getLargestX(s));
    
    }
    // This method prints names of all files in a chosen folder that you can use to test your other methods
    public void printFileNames() {
        DirectoryResource dr = new DirectoryResource();
        for (File f : dr.selectedFiles()) {
            System.out.println(f);
        }
    }

    public static void main (String[] args) {
        PerimeterAssignmentRunner pr = new PerimeterAssignmentRunner();
        pr.testPerimeter();
    }
}

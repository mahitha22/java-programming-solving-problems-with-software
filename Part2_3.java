
/**
 * Write a description of Part3 here.
 * 
 * @author Mahitha 
 * Strings Assignment 2 Part 3
 * 
 */
public class Part2_3 {

public int findStopCodon (String dna, int startIndex, String stopCodon) {
 int currIndex = dna.indexOf(stopCodon,startIndex + 3);
 while(currIndex != -1) {
  if((currIndex - startIndex) % 3 == 0) {
   return currIndex;
  }
  else {
    currIndex = dna.indexOf(stopCodon,currIndex+1);    
  }
}
return -1;
}

public  String findSimpleGene(String dna, int where) {
    int startIndex = dna.indexOf("ATG", where);
    if(startIndex == -1) {
      return "";
    }
    int taaIndex = findStopCodon(dna, startIndex, "TAA");
    int tgaIndex = findStopCodon(dna, startIndex, "TGA");
    int tagIndex = findStopCodon(dna, startIndex, "TAG");
    int minIndex = 0;
    if(taaIndex == -1 || (tgaIndex != -1 && tgaIndex < taaIndex)) {
        minIndex = tgaIndex;
    }
    else {
        minIndex = taaIndex;
    }
    if(minIndex == -1 || (tagIndex != -1 && tagIndex < minIndex)) {
        minIndex = tagIndex;
    }
    //int minIndex = Math.min(taaIndex,(Math.min(tgaIndex,tagIndex)));
    if(minIndex == -1) {
       return ""; 
    }
    return dna.substring(startIndex,minIndex + 3);
    
}
public void countAllGenes(String dna) {
    int startIndex1 = 0;
    int count = 0;
    while( true ){
        String currGene = findSimpleGene(dna, startIndex1);
        if(currGene.isEmpty()) {
            break;
        }
        else {
        //System.out.println(currGene);
        startIndex1 = dna.indexOf(currGene, startIndex1) + currGene.length();
        count ++;
    }
    }
    System.out.println(count);
}
public void testOn() {
               //   =   =  =  =           -  -  -  -      9  9  9  9 
    String DNA = "ATGATCCTATAAGHCVBNTYUATGHJUIKLTAGGHVCATGJHBGTTAATAG";
    countAllGenes(DNA);
     DNA = "";
    countAllGenes(DNA);
    countAllGenes("ATGTAAGATGCCCTAGT");
}
public void findAbc(String input){
       int index = input.indexOf("abc");
       
       while (true){
           if (index == -1 || index >= input.length() - 3){ 
               break;
           }
           //System.out.println(index);
           String found = input.substring(index+1, index+4);
           System.out.println(found);
           index = input.indexOf("abc",index+3);
           //System.out.println("index after updating " + index);
       }
}
public void test() {
  //findAbc("abcbbbabcdddabc");
  //findAbc("eusabce");
  //findAbc("yabcyabc");
   //findAbc("abcdabc");
  //findAbc("woiehabchi");
 // findAbc("aaaaabc");
  findAbc("abcabcabcabca");
}
}      

/**
 * Write a description of Part1 here.
 * 
 * @author (your name) 
 * Strings Assignment 2 - Part 1
 * @version (a version number or a date)
 */
public class Part2_1 {

public int findStopCodon (String dna, int startIndex, String stopCodon) {
 int currIndex = dna.indexOf(stopCodon,startIndex + 3);
 while(currIndex != -1) {
  if((currIndex - startIndex) % 3 == 0) {
   return currIndex;
  }
  else {
    currIndex = dna.indexOf(stopCodon,currIndex+1);    
  }
}
return dna.length();
}

public  String findSimpleGene(String dna, int where) {
    int startIndex = dna.indexOf("ATG", where);
    if(startIndex == -1) {
      return "";
    }
    int taaIndex = findStopCodon(dna, startIndex, "TAA");
    int tgaIndex = findStopCodon(dna, startIndex, "TGA");
    int tagIndex = findStopCodon(dna, startIndex, "TAG");
   /* int minIndex = 0;
    if(taaIndex == -1 || (tgaIndex != -1 && tgaIndex < taaIndex)) {
        minIndex = tgaIndex;
    }
    else {
        minIndex = taaIndex;
    }
    if(minIndex == -1 || (tagIndex != -1 && tagIndex < minIndex)) {
        minIndex = tagIndex;
    }*/
    int minIndex = Math.min(taaIndex,(Math.min(tgaIndex,tagIndex)));
    if (minIndex == dna.length()) {
        return "";
    }
    else
    //if(minIndex == -1) {
     //  return ""; 
  //  }
    return dna.substring(startIndex,minIndex + 3);
    
}
/*public  void testSimpleGene() {
        
        String DNA = "ATGCGTAATATGGT";
        System.out.println("DNA Strand is: "+DNA);
        String gene = findSimpleGene(DNA);
        System.out.println("Gene is "+gene);
        //         v  v  v  v  
        DNA = "AATGCTAGGGTAATATGGT";
        System.out.println("DNA Strand is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("Gene is "+gene);
          //      v  v  v  v  v  v  v  v  v  v  v
        DNA = "AACATGVVCCVBBBXTAGBVCXSDTGABNMTAA";
        System.out.println("DNA Strand is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("Gene is "+gene);
        
        DNA = "ATGTAG";
        System.out.println("DNA Strand is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("Gene is "+gene);
        
        DNA = "ATGTTGTAGTAA";
        System.out.println("DNA Strand is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("Gene is"+gene);
        
        DNA = "TTAATGGSTTGATAA";
        System.out.println("DNA Strand is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("Gene is"+gene);
        
        DNA = "AATGATG";
        System.out.println("DNA Strand is: "+DNA);
        gene = findSimpleGene(DNA);
        System.out.println("Gene is"+gene);
}
public void testFindStopCodon() {
    String dna = "xxxyyyzzzTAAxxxyyyzzzTAAxx";
    int dex = findStopCodon(dna,0,"TAA");
    if(dex != 9) System.out.println("error on 9");
    dex = findStopCodon(dna,9,"TAA");
    if(dex != 21) System.out.println("error on 21");
    dex = findStopCodon(dna,1,"TAA");
    if(dex != 26) System.out.println("error on 26");
    dex = findStopCodon(dna,0, "TAG");
    if(dex != 26) System.out.println("error on 26");
    System.out.println("Tests finished");
}*/
public void printAllGenes(String dna) {
    int startIndex = 0;
    while( true ){
        String currGene = findSimpleGene(dna, startIndex);
        if(currGene.isEmpty()) {
            break;
        }
        System.out.println(currGene);
        startIndex = dna.indexOf(currGene, startIndex) + currGene.length();
        //System.out.println(startIndex);
    }
}
public void testOn() {
               //   =   =  =  =           -  -  -  -      9  9  9  9 
    String DNA = "ATGATCCTATAAGHCVBNTYUATGHJUIKLTAGGHVCATGJHBGTTAATAG";
    printAllGenes(DNA);
     DNA = "";
    printAllGenes(DNA);
}
}      
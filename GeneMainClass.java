public class GeneMainClass {
    public  String findSimpleGene(String dna, int startIndex, int stopIndex) {
        //int startCodonIndex = dna.indexOf("ATG");
        //int stopCodonIndex = dna.indexOf("TAA", startCodonIndex + 3);
         String result = "";
        if(startIndex == -1) {
            return " ";
        }
       
        if(stopIndex == -1) {
            return " ";
        }
        //if(((dna.substring(startCodonIndex,stopCodonIndex)).length())/3 == 0) {
            result = dna.substring(startIndex, stopIndex+3);
        //}
        return result;
    }
    
    public  void testSimpleGene() {
        
        String DNA = "AATGCGTAATATGGT";
        System.out.println("DNA Strand is: "+DNA);
        int startCodonIndex = DNA.indexOf("ATG");
        int stopCodonIndex = DNA.indexOf("TAA", startCodonIndex + 3);
        String gene = findSimpleGene(DNA, startCodonIndex, stopCodonIndex);
        System.out.println("Gene is "+gene);
        
        DNA = "AATGCTAGGGTAATATGGT";
        System.out.println("DNA Strand is: "+DNA);
        startCodonIndex = DNA.indexOf("ATG");
        stopCodonIndex = DNA.indexOf("TAA", startCodonIndex + 3);
        gene = findSimpleGene(DNA, startCodonIndex, stopCodonIndex);
        System.out.println("Gene is "+gene);
        
        DNA = "ATCCTATGCTTCGGCTGCTCTAATATGGT";
        System.out.println("DNA Strand is: "+DNA);
        startCodonIndex = DNA.indexOf("ATG");
        stopCodonIndex = DNA.indexOf("TAA", startCodonIndex + 3);
        gene = findSimpleGene(DNA, startCodonIndex, stopCodonIndex);
        System.out.println("Gene is "+gene);
        
        DNA = "ATGTAA";
        System.out.println("DNA Strand is: "+DNA);
        startCodonIndex = DNA.indexOf("ATG");
        stopCodonIndex = DNA.indexOf("TAA", startCodonIndex + 3);
        gene = findSimpleGene(DNA, startCodonIndex, stopCodonIndex);
        System.out.println("Gene is "+gene);
        
        DNA = "TTGTAGTAA";
        System.out.println("DNA Strand is: "+DNA);
        startCodonIndex = DNA.indexOf("ATG");
        stopCodonIndex = DNA.indexOf("TAA", startCodonIndex + 3);
        gene = findSimpleGene(DNA, startCodonIndex, stopCodonIndex);
        System.out.println("Gene is"+gene);
        
        DNA = "TTAATG";
        System.out.println("DNA Strand is: "+DNA);
        startCodonIndex = DNA.indexOf("ATG");
        stopCodonIndex = DNA.indexOf("TAA", startCodonIndex + 3);
        gene = findSimpleGene(DNA, startCodonIndex, stopCodonIndex);
        System.out.println("Gene is"+gene);
        
        DNA = "AATGATG";
        System.out.println("DNA Strand is: "+DNA);
        startCodonIndex = DNA.indexOf("ATG");
        stopCodonIndex = DNA.indexOf("TAA", startCodonIndex + 3);
        gene = findSimpleGene(DNA, startCodonIndex, stopCodonIndex);
        System.out.println("Gene is"+gene);
          
    }

    public static void main(String args[]) {

        GeneMainClass g = new GeneMainClass();
        g.testSimpleGene();
    }
}
